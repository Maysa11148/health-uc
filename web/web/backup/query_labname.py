import pymongo as mongo
import urllib
import re
from bson.objectid import ObjectId
import datetime
import json

client = mongo.MongoClient("mongodb://bypanon.su:"+urllib.parse.quote("panon.su@123")+"@192.168.10.24:27018/arcusairdb")
db = client.arcusairdb

# All distinc lab name
allLabName = db.patientvisits.aggregate([
    {
        "$lookup":
        {
            "from": "labresults",
            "localField": "_id",
            "foreignField": "patientvisituid",
            "as": "labresult"
        }
    },
    {"$unwind": "$labresult"},
    {
        "$lookup":
        {
            "from": "orderitems",
            "localField": "labresult.orderitemuid",
            "foreignField": "_id",
            "as": "lab"
        }
    },
    {"$unwind": "$lab"},
    {"$unwind": "$labresult.resultvalues"},
    { "$group" : { "_id" : "$lab.name" } },
    {
        "$project":
        {
            "_id": 1
        }

    }
])
print(len([x['_id'] for x in allLabName]))