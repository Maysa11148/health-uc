import pymongo as mongo
import urllib
import re
from bson.objectid import ObjectId
import datetime

def pipeline1():
    REGEX = re.compile("E1.")
    return [
    {
        "$lookup": {
            "from": "patients",
            "localField": "patientuid",
            "foreignField": "_id",
            "as": "patient"
        }
    },
    {"$unwind": "$patient"},
    {
        "$lookup": {
            "from": "diagnoses",
            "localField": "_id",
            "foreignField": "patientvisituid",
            "as": "diagnosis"
        }
    },
    {"$unwind": "$diagnosis"},
    {"$unwind": "$diagnosis.diagnosis"},
    {
        "$lookup": {
            "from": "problems",
            "localField": "diagnosis.diagnosis.problemuid",
            "foreignField": "_id",
            "as": "problem"
        }
    },
    {"$unwind": "$problem"},
    {
        "$match": {
            "problem.code": {"$regex": REGEX}
        }
    },
    {
        "$project":
        {
            "_id": 0,
            "id": "$patient._id"
        }
    }
]


def pipeline2(obj):
    return [
    {
        "$match": {"_id": ObjectId(obj)}
    },
    {
        "$lookup": {
            "from": "patientvisits",
            "localField": "_id",
            "foreignField": "patientuid",
            "as": "patientvisit"
        }
    },
    {"$unwind": "$patientvisit"},
    {
        "$lookup": {
            "from": "referencevalues",
            "localField": "genderuid",
            "foreignField": "_id",
            "as": "gender"
        }
    },
    {
        "$unwind":
        {
            "path": "$gender",
            "preserveNullAndEmptyArrays": True
        }
    },
        {
        "$lookup":
        {
            "from": "cchpis",
            "localField": "patientvisit._id",
            "foreignField": "patientvisituid",
            "as": "cchpi"
        }
    },
    {
        "$unwind":
        {
            "path": "$cchpi",
            "preserveNullAndEmptyArrays": True
        }
    },
    {
        "$unwind":
        {
            "path": "$cchpi.cchpis",
            "preserveNullAndEmptyArrays": True
        }
    },
    {
        "$lookup":
        {
            "from": "examinations",
            "localField": "patientvisit._id",
            "foreignField": "patientvisituid",
            "as": "examination"
        }
    },
    {
        "$unwind":
        {
            "path": "$examination",
            "preserveNullAndEmptyArrays": True
        }
    },
    {
        "$lookup": {
            "from": "diagnoses",
            "localField": "patientvisit._id",
            "foreignField": "patientvisituid",
            "as": "diagnosis"
        }
    },
    {"$unwind": "$diagnosis"},
    {"$unwind": "$diagnosis.diagnosis"},
    {
        "$lookup": {
            "from": "problems",
            "localField": "diagnosis.diagnosis.problemuid",
            "foreignField": "_id",
            "as": "problem"
        }
    },
    {"$unwind": "$problem"},
    {
        "$match":
        {
            "diagnosis.diagnosis.isprimary": True
        }
    },
    {
        "$project": {
            "_id": 0,
            "patientvisituid": "$patientvisit._id",
            "HN": "$patient.mrn",
            "dob": {"$ifNull": ["$dateofbirth", ""]},
            "gender": {"$ifNull": ["$gender.locallanguagedesc", ""]},
            "startDTTM": "$startdate",
            "chief_complaint":
            {
                "$switch":
                {
                    "branches":
                    [
                        {
                            "case": {"$eq": ["$cchpi.statusflag", "A"]},
                            "then": "$cchpi.cchpis.chiefcomplaint"
                        }
                    ],
                    "default": None
                }
            },
            "present_illness":
            {
                "$switch":
                {
                    "branches":
                    [
                        {
                            "case": {"$eq": ["$cchpi.statusflag", "A"]},
                            "then": "$cchpi.presentillness"
                        }
                    ],
                    "default": None
                }
            },
            "physical_exam":
            {
                "$switch":
                {
                    "branches":
                    [
                        {
                            "case": {"$eq": ["$examination.statusflag", "A"]},
                            "then": "$examination.examinationtext"
                        }
                    ],
                    "default": None
                }
            },
            "PRINCIPLE_DX_Code": "$problem.code",
            "PRINCIPLE_DX": "$problem.name",
            "HN": "$mrn"
        }
    },
]


def pipeline3(obj):
    return [
    {
        "$match": {"_id": ObjectId(obj)}
    },
    {
        "$lookup":
        {
            "from": "labresults",
            "localField": "_id",
            "foreignField": "patientvisituid",
            "as": "labresult"
        }
    },
    {"$unwind": "$labresult"},
    {
        "$lookup":
        {
            "from": "orderitems",
            "localField": "labresult.orderitemuid",
            "foreignField": "_id",
            "as": "lab"
        }
    },
    {"$unwind": "$lab"},
    {"$unwind": "$labresult.resultvalues"},
    {
        "$project":
        {
            "_id": 0,
            "lab_name": "$lab.name",
            "result_name": "$labresult.resultvalues.name",
            "result": "$labresult.resultvalues.resultvalue",
        }

    },
]


client = mongo.MongoClient("mongodb://bypanon.su:"+urllib.parse.quote("panon.su@123")+"@192.168.10.24:27018/arcusairdb")
db = client.arcusairdb
f = open("query2.csv", "a", encoding="utf-16")
delimiter = "###"

patientuid_set = set()
for x in [x for x in db.patientvisits.aggregate(pipeline1())]:
    patientuid_set.add(x["id"])


for patient in patientuid_set:
    visits = [x for x in db.patients.aggregate(pipeline2(patient))]
    for visit in visits:
        print("--------------------------------------------")
        print(visit["patientvisituid"])
        labs = [x for x in db.patientvisits.aggregate(pipeline3(visit["patientvisituid"]))]
        visit["lab"] = []
        for lab in labs:
            if "result" in lab:
                visit["lab"].append(lab["lab_name"]+"-"+lab["result_name"]+":"+str(lab["result"]))
        visit["lab"] = " | ".join(visit["lab"])
        visit["dob"] = visit["dob"].strftime("%m/%d/%Y %H:%M:%S")

        tmp = visit['HN'] + delimiter \
              + visit['dob'] + delimiter \
              + visit['gender'] + delimiter
        try: tmp += visit['PRINCIPLE_DX_Code'] + delimiter
        except: tmp += delimiter
        try: tmp += visit['chief_complaint'] + delimiter
        except: tmp += delimiter
        try: tmp += visit['present_illness'] + delimiter
        except: tmp += delimiter
        try: tmp += visit['physical_exam'] + delimiter
        except: tmp += delimiter
        try: tmp += visit['PRINCIPLE_DX'] + delimiter
        except: tmp += delimiter
        try: tmp += visit["lab"] + delimiter
        except: tmp += delimiter

        tmp = tmp.replace("\n", " ")
        f.write(tmp)
        f.write("\n")

