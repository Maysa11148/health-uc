import csv
import json
import logging

from flask import Flask, abort, jsonify, render_template, request

from uc import analysis


app = Flask(__name__)
logger = logging.getLogger(__name__)


with open('raw/alllabgroup.csv', 'r', encoding='utf8') as f:
    reader = csv.reader(f)
    lab_groups = list(reader)[0]

with open('raw/alldic.json') as json_file:
    dic = json.load(json_file)

with open('raw/unitdic.json') as json_file:
    unit = json.load(json_file)


@app.route('/', methods=['GET'])
def uc_main_pange():
    return render_template('index.html')   


@app.route('/uc', methods=['POST'])
def uc_analysis():
    if request.method == 'POST':
        data = request.json
        result = analysis.getresult(data)
        return jsonify({"result": result})

@app.route('/lab-groups', methods=['GET'])
def get_lab_groups():
    if request.method == 'GET':
        return jsonify({"result": lab_groups})


@app.route('/diclab', methods=['GET'])
def get_lab_dic():
    if request.method == 'GET':
        return jsonify({"result": dic})


@app.route('/dicunit', methods=['GET', 'POST'])
def get_unit_dic():
    if request.method == 'GET':
        return jsonify({"result": unit})
    if request.method == 'POST':
        data = request.json
        if len(unit[data['labgroup']])<1 or data['labname'] not in unit[data['labgroup']].keys() or len(unit[data['labgroup']][data['labname']]) < 1:
            return jsonify({"result": '<div class="form-group"><span for="unit">none</span></div>'})
        elif len(unit[data['labgroup']][data['labname']]) == 1:
            return jsonify({"result": '<div class="form-group"><span for="unit">'+ unit[data['labgroup']][data['labname']][0] +'</span></div>'})
        else:
            unitoption = ''
            for i in unit[data['labgroup']][data['labname']]:
                unitoption += '<option value="' + i + '">'+ i +'</option>'
            return jsonify({"result": '<select name="lab_names[]" class="browser-default custom-select lab_names">' + unitoption + '</select>'})


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)
