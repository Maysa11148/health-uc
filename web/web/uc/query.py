import pymongo as mongo
import urllib
import re
from bson.objectid import ObjectId
import datetime
import pandas as pd
import json

def history(hn):
    def OPD_visit_pipeline(hn):
        return [
        {
            "$lookup": {
                "from": "patients",
                "localField": "patientuid",
                "foreignField": "_id",
                "as": "patient"
            }
        },
        {
            "$unwind":'$patient'
        },
        {
            "$match":
            {
                "patient.mrn": hn
            }
        },
        {
            "$project":
            {
                "_id": 1
            }
        }
    ]


    def patient_pipeline(obj):
        return [
        {
            "$match": {"_id": ObjectId(obj)}
        },
        {
            "$lookup": {
                "from": "patients",
                "localField": "patientuid",
                "foreignField": "_id",
                "as": "patient"
            }
        },
        {"$unwind": "$patient"},
        {
            "$lookup": {
                "from": "referencevalues",
                "localField": "patient.genderuid",
                "foreignField": "_id",
                "as": "gender"
            }
        },
        {
            "$unwind":
            {
                "path": "$gender",
                "preserveNullAndEmptyArrays": True
            }
        },
        {
            "$lookup": {
                "from": "referencevalues",
                "localField": "entypeuid",
                "foreignField": "_id",
                "as": "entype"
            }
        },
        {
            "$unwind":
            {
                "path": "$entype",
                "preserveNullAndEmptyArrays": True
            }
        },
        {
            "$project": {
                "_id": 0,
                "HN": "$patient.mrn",
                "visitType": "$entype.valuedescription",
                "dob":
                {
                    "$ifNull":
                    [
                        {
                            "$divide": [{"$subtract": ["$createdat","$patient.dateofbirth"]}, 86400000]
                        },
                        ""
                    ]
                },
                "gender": {"$ifNull": ["$gender.locallanguagedesc", ""]},
                "visitid": "$visitid",
                "create_date": '$createdat'
            }
        },
    ]


    def chief_complaint_pipeline(obj):
        return [
        {
            "$match": {"patientvisituid": ObjectId(obj)}
        },
        {"$unwind": "$cchpis"},
        {
            "$project":
            {
                "_id": 0,
                "chief_complaint":
                {
                    "$switch":
                    {
                        "branches":
                        [
                            {
                                "case": {"$eq": ["$statusflag", "A"]},
                                "then": "$cchpis.chiefcomplaint"
                            }
                        ],
                        "default": None
                    }
                },
            }
        }
    ]


    def present_illness_pipeline(obj):
        return [
        {
            "$match": {"patientvisituid": ObjectId(obj)}
        },
        {
            "$project":
            {
                "_id": 0,
                "present_illness":
                {
                    "$switch":
                    {
                        "branches":
                        [
                            {
                                "case": {"$eq": ["$statusflag", "A"]},
                                "then": "$presentillness"
                            }
                        ],
                        "default": None
                    }
                },
            }
        }
    ]


    def physical_exam_pipeline(obj):
        return [
        {
            "$match": {"patientvisituid": ObjectId(obj)}
        },
        {
            "$project":
            {
                "_id": 0,
                "physical_exam":
                {
                    "$switch":
                    {
                        "branches":
                        [
                            {
                                "case": {"$eq": ["$statusflag", "A"]},
                                "then": "$examinationtext"
                            }
                        ],
                        "default": None
                    }
                },
            }
        }
    ]


    def primary_diag_pipeline(obj):
        return [
        {
            "$match": {"patientvisituid": ObjectId(obj)}
        },
        {"$unwind": "$diagnosis"},
        {
            "$lookup": {
                "from": "problems",
                "localField": "diagnosis.problemuid",
                "foreignField": "_id",
                "as": "problem"
            }
        },
        {"$unwind": "$problem"},
        {
            "$match":
            {
                "diagnosis.isprimary": True
            }
        },
        {
            "$match":
            {
                "diagnosis.isprimary": True
            }
        },
        {
            "$project":
            {
                "_id": 0,
                "PRINCIPLE_DX_Code": "$problem.code",
                "PRINCIPLE_DX": "$problem.name",
                "PRINCIPLE_DX_Detail": "$diagnosis.comments"
            }
        },
    ]

    def lab_pipeline(obj):
        return [
        {
            "$match": {"_id": ObjectId(obj)}
        },
        {
            "$lookup":
            {
                "from": "labresults",
                "localField": "_id",
                "foreignField": "patientvisituid",
                "as": "labresult"
            }
        },
        {"$unwind": "$labresult"},
        {
            "$lookup":
            {
                "from": "orderitems",
                "localField": "labresult.orderitemuid",
                "foreignField": "_id",
                "as": "lab"
            }
        },
        {"$unwind": "$lab"},
        {"$unwind": "$labresult.resultvalues"},
        {
            "$project":
            {
                "_id": 0,
                "lab_name": "$lab.name",
                "result_name": "$labresult.resultvalues.name",
                "result": "$labresult.resultvalues.resultvalue",
            }

        },
    ]

    def como_diag_pipeline(obj):
        return [
        {
            "$match": {"_id": ObjectId(obj)}
        },
        {
            "$lookup": {
                "from": "diagnoses",
                "localField": "_id",
                "foreignField": "patientvisituid",
                "as": "diagnosis"
            }
        },
        {"$unwind": "$diagnosis"},
        {"$unwind": "$diagnosis.diagnosis"},
        {
            "$lookup": {
                "from": "problems",
                "localField": "diagnosis.diagnosis.problemuid",
                "foreignField": "_id",
                "as": "problem"
            }
        },
        {"$unwind": "$problem"},
        {
            "$match":
            {
                "diagnosis.diagnosis.comorbidityuid": {"$ne": None}
            }
        },
        {
            "$project": {
                "_id": 0,
                "CO_MORBIDITY_Code": "$problem.code",
                "CO_MORBIDITY": "$problem.name",
                "CO_MORBIDITY_Detail": "$diagnosis.diagnosis.comments"
            }
        },
    ]


    def other_diag_pipeline(obj):
        return [
        {
            "$match": {"_id": ObjectId(obj)}
        },
        {
            "$lookup": {
                "from": "diagnoses",
                "localField": "_id",
                "foreignField": "patientvisituid",
                "as": "diagnosis"
            }
        },
        {"$unwind": "$diagnosis"},
        {"$unwind": "$diagnosis.diagnosis"},
        {
            "$lookup": {
                "from": "problems",
                "localField": "diagnosis.diagnosis.problemuid",
                "foreignField": "_id",
                "as": "problem"
            }
        },
        {"$unwind": "$problem"},
        {
            "$match":
            {
                "diagnosis.diagnosis.isprimary": False,
                "diagnosis.diagnosis.comorbidityuid": {"$eq": None}
            }
        },
        {
            "$project": {
                "_id": 0,
                "other_Code": "$problem.code",
                "other": "$problem.name",
                "other_Detail": "$diagnosis.diagnosis.comments"
            }
        },
    ]


    def OPD_observation_pipeline(obj):
        return [
        {
            "$match": {"patientvisituid": ObjectId(obj)},
        },
        {"$sort": {"statusflag": 1, "createdat": 1}},
        {"$limit": 1},
        {"$unwind": "$observationvalues"},
        {
            "$project":
            {
                "_id": 0,
                "name": "$observationvalues.name",
                "resultvalue": "$observationvalues.resultvalue"
            }
        }
    ]


    client = mongo.MongoClient("mongodb://insclaim:"+urllib.parse.quote("insclaim@123")+"@192.168.10.24:27018/arcusairdb")
    db = client.arcusairdb

    columns = ['HN', 'visitid', 'visitType', 'dob', 'gender', 'chief_complaint', 'present_illness','physical_exam'
                  , 'PRINCIPLE_DX_Code', 'PRINCIPLE_DX', 'PRINCIPLE_DX_Detail', 'CO_MORBIDITY_Code', 'CO_MORBIDITY'
                  , 'CO_MORBIDITY_Detail', "other_Code", "other", "other_Detail", "lab", "Weight", "Height", "Systolic_BP"
                  , "Diastolic_BP", 'createdat']
    dic = {}
    for i in columns:
        dic[i] = []


    OPD_visits = [x for x in db.patientvisits.aggregate(OPD_visit_pipeline(hn))]


    for OPD_visit in OPD_visits:

        visit = [x for x in db.patientvisits.aggregate(patient_pipeline(OPD_visit["_id"]))][0]

        try: visit["dob"] = str(int(visit["dob"]))
        except : pass

        visit["chief_complaint"] = []
        complains = [x for x in db.cchpis.aggregate(chief_complaint_pipeline(OPD_visit["_id"]))]
        for complain in complains:
            if "chief_complaint" in complain and len(complain["chief_complaint"].strip()) != 0:
                visit["chief_complaint"].append(complain["chief_complaint"])
        visit["chief_complaint"] = "; ".join(visit["chief_complaint"])

        visit["present_illness"] = []
        illness = [x for x in db.cchpis.aggregate(present_illness_pipeline(OPD_visit["_id"]))]
        for ill in illness:
            if "present_illness" in ill and len(ill["present_illness"].strip()) != 0:
                visit["present_illness"].append(ill["present_illness"])
        visit["present_illness"] = "; ".join(visit["present_illness"])

        visit['physical_exam'] = []
        exams = [x for x in db.examinations.aggregate(physical_exam_pipeline(OPD_visit["_id"]))]
        for exam in exams:
            if 'physical_exam' in exam and len(exam['physical_exam'].strip()) != 0:
                visit['physical_exam'].append(exam['physical_exam'])
        visit['physical_exam'] = "; ".join(visit['physical_exam'])

        visit['PRINCIPLE_DX_Code'] = []
        visit['PRINCIPLE_DX'] = []
        visit['PRINCIPLE_DX_Detail'] = []
        primary_diagnoses = [x for x in db.diagnoses.aggregate(primary_diag_pipeline(OPD_visit["_id"]))]
        for diag in primary_diagnoses:
            if 'PRINCIPLE_DX_Code' in diag:
                visit['PRINCIPLE_DX_Code'].append(diag['PRINCIPLE_DX_Code'])
            if 'PRINCIPLE_DX' in diag:
                visit['PRINCIPLE_DX'].append(diag['PRINCIPLE_DX'])
            if 'PRINCIPLE_DX_Detail' in diag and len(diag['PRINCIPLE_DX_Detail'].strip()) != 0:
                visit['PRINCIPLE_DX_Detail'].append(diag['PRINCIPLE_DX_Detail'])
        visit['PRINCIPLE_DX_Code'] = "; ".join(visit['PRINCIPLE_DX_Code'])
        visit['PRINCIPLE_DX'] = "; ".join(visit['PRINCIPLE_DX'])
        visit['PRINCIPLE_DX_Detail'] = "; ".join(visit['PRINCIPLE_DX_Detail'])

        labs = [x for x in db.patientvisits.aggregate(lab_pipeline(OPD_visit["_id"]))]
        visit["lab"] = []
        for lab in labs:
                if "result" in lab:
                    visit["lab"].append({
                        "lab_name": lab["lab_name"],
                        "result_name": lab["result_name"],
                        "result": str(lab["result"])
                    })
        visit["lab"] = json.dumps(visit["lab"], ensure_ascii=False)

        visit["CO_MORBIDITY_Code"] = []
        visit["CO_MORBIDITY"] = []
        visit["CO_MORBIDITY_Detail"] = []
        como_diagnoses = [x for x in db.patientvisits.aggregate(como_diag_pipeline(OPD_visit["_id"]))]
        for diag in como_diagnoses:
            if "CO_MORBIDITY_Code" in diag:
                visit["CO_MORBIDITY_Code"].append(diag["CO_MORBIDITY_Code"])
            if "CO_MORBIDITY" in diag:
                visit["CO_MORBIDITY"].append(diag["CO_MORBIDITY"])
            if "CO_MORBIDITY_Detail" in diag and len(diag["CO_MORBIDITY_Detail"].strip()) != 0:
                visit["CO_MORBIDITY_Detail"].append(diag["CO_MORBIDITY_Detail"])
        visit["CO_MORBIDITY_Code"] = "; ".join(visit["CO_MORBIDITY_Code"])
        visit["CO_MORBIDITY"] = "; ".join(visit["CO_MORBIDITY"])
        visit["CO_MORBIDITY_Detail"] = "; ".join(visit["CO_MORBIDITY_Detail"])

        visit["other_Code"] = []
        visit["other"] = []
        visit["other_Detail"] = []
        como_diagnoses = [x for x in db.patientvisits.aggregate(other_diag_pipeline(OPD_visit["_id"]))]
        for diag in como_diagnoses:
            if "other_Code" in diag:
                visit["other_Code"].append(diag["other_Code"])
            if "other" in diag:
                visit["other"].append(diag["other"])
            if "other_Detail" in diag and len(diag["other_Detail"].strip()) != 0:
                visit["other_Detail"].append(diag["other_Detail"])
        visit["other_Code"] = "; ".join(visit["other_Code"])
        visit["other"] = "; ".join(visit["other"])
        visit["other_Detail"] = "; ".join(visit["other_Detail"])

        visit["Weight"] = ""
        visit["Height"] = ""
        visit["Systolic_BP"] = ""
        visit["Diastolic_BP"] = ""
        observations = [x for x in db.observations.aggregate(OPD_observation_pipeline(OPD_visit["_id"]))]
        for observation in observations:
            if observation["name"] == "Weight" and "resultvalue" in observation:
                visit["Weight"] = observation["resultvalue"]
            if observation["name"] == "Height" and "resultvalue" in observation:
                visit["Height"] = observation["resultvalue"]
            if observation["name"] == "Systolic BP" and "resultvalue" in observation:
                visit["Systolic_BP"] = observation["resultvalue"]
            if observation["name"] == "Diastolic BP" and "resultvalue" in observation:
                visit["Diastolic_BP"] = observation["resultvalue"]

        fields = ['visitid', 'visitType', 'dob', 'gender', 'chief_complaint', 'present_illness','physical_exam'
                  , 'PRINCIPLE_DX_Code', 'PRINCIPLE_DX', 'PRINCIPLE_DX_Detail', 'CO_MORBIDITY_Code', 'CO_MORBIDITY'
                  , 'CO_MORBIDITY_Detail', "other_Code", "other", "other_Detail", "lab", "Weight", "Height", "Systolic_BP"
                  , "Diastolic_BP"]
        dic['HN'].append(visit['HN'])
        for field in fields:
            try: dic[field].append(visit[field])
            except: dic[field].append('')
        try: dic['createdat'].append(visit["create_date"])
        except: dic['createdat'].append('')

    return dic