import json
import pandas as pd
import numpy as np

from uc import query, check


def getresult(data):
    dic = query.history(data['HN'])
    lab = [{'lab_name': a, 'result_name': b, 'result': c} for a,b,c in list(zip(data['lab_groups'], data['lab_names'], data['lab_results']))]
    used_cols = list(data.keys())

    for i in ['lab_groups', 'lab_names', 'lab_results']:
        used_cols.remove(i)

    for i in dic.keys():
        if i in used_cols:
            dic[i].append(data[i])
        elif i == 'lab':
            dic['lab'].append(lab)
        else:
            dic[i].append('')
    
    df=pd.DataFrame(dic)
    df['createdat'] = pd.to_datetime(df['createdat'])
    df['lab']=df['lab'].map(lambda x: json.loads(x) if type(x)==str else x)
    result = check.e1x2(df, len(df)-1)
    
    head = []
    body = []

    if result['result'] == 'Fail':
        result = result['detail']
        for option, dic in result.items():
            head.append('<th scope="col">' + option + ' : Fail</th>')
            col = []
            for cond, lis in dic.items():
                string = '<td>' + cond + '?'
                if lis[0] == True:
                    string += '<br><i class="fas fa-check"></i>'
                else:
                    string += '<br><i class="fas fa-times"></i>'
                if len(lis) > 1:
                    string += '<br><b>comment : </b>' + lis[1]
                string += '</td>'
                col.append(string)
            body.append(col)
        
        headcode = ''
        for i in head:
            headcode += i

        bodycode = ''
        for i in body:
            bodycode += i[0]

        table = '<table class="table"><thead><tr>'+ headcode + '</tr></thead><tbody><tr>' + bodycode + '</tr><tr>' + body[0][1] + '<td></td><td></td><td></td></tr><tr>' + body[0][2]*4 + '</tr><tr>' + body[0][3]*4 + '</tr></tbody></table>'
    
    else:
        head.append('<th scope="col">' + 'Pass' + '</th>')
        col = []
        for cond, lis in result['detail'].items():
            string = '<td>' + cond + '?'
            if lis[0] == True:
                string += '<br><i class="fas fa-check"></i>'
            else:
                string += '<br><i class="fas fa-times"></i>'
            if len(lis) > 1:
                string += '<br><b>comment : </b>' + lis[1]
            string += '</td>'
            col.append(string)
        body.append(col)

        headcode = ''
        for i in head:
            headcode += i

        table = '<table class="table"><thead><tr>'+ headcode + '</tr></thead><tbody>'
        for i in body[0]:
            table += '<tr>' + i + '</tr>'
        table += '</tbody></table>'

    return table