import re
import pandas as pd
import numpy as np

def e1x2(df, index):
    
#     update_progress(index/len(df))
    
    # Condition-function---------------------------------------------------------------------------#
    
    # 1. History check: ---------------------------------------------------------------------------#
    
    histindex = []
    for i in range(len(df)):
        if df['HN'][i] == df['HN'][index] and df['createdat'][i] <= df['createdat'][index]:
            histindex.append(i)
                
    def histrenal(df, index):
        histindexx = [x for x in histindex if x != index]
        result = [False]
        for j in histindexx:
            if type(df['PRINCIPLE_DX_Code'][j]) == str and df['PRINCIPLE_DX_Code'][j][:2]=='E1' and df['PRINCIPLE_DX_Code'][j][-1] == '2':
                result = [True, 'in PRINCIPLE_DX_Code at visitid '+str(df['visitid'][j])]
                break
            elif type(df['other_Code'][j]) == str:
                for k in df['other_Code'][j].split(';'):
                    if k[:2]=='E1' and k[-1] == '2':
                        result = [True, 'in other_Code at visitid '+str(df['visitid'][j])]
                        break
            if result[0] == True:
                break
        return result
    
    # 2. Lab result-----------------------------------------------------------------------------------#
    
    # urine protein
    strip = ['negative','trace','1+','2+','3+','4+']
    non = ['none','-','']
    #else: are not from test strip

    def geturineprotine(df, index):
        if type(df['lab'][index]) == list and len(df['lab'][index]) > 0:
            result = []
            for dic in df['lab'][index]:
                ln = dic['lab_name'].lower()
                rn = dic['result_name'].lower()
                if 'urine' in ln and 'protein' in rn:
                    if dic['result'].replace(' ','').lower() in strip:
                        return dic['result'].replace(' ','').lower()
                    result.append(dic['result'].replace(' ','').lower())
                elif df['lab'][index][-1] == dic and len(result)==0:
                    return 'none'
            if len(result)>0 :
                return result[0]
        else:
            return 'none'
    
    def urineprotein_num(df, index, cutoff, time):
        #pass
        num = 0
        visitid = []
        #fail
        fail = 0
        failid = []
        #are not from test strip
        cannot = 0
        unpred = []
        #no lab
        no = 0
        for j in histindex:
            if geturineprotine(df, j) in strip:
                if cutoff == '1+':
                    if geturineprotine(df, j) in ['1+','2+','3+','4+','positive']:
                        num+=1
                        visitid.append(df['visitid'][j])
                    else:
                        fail+=1
                        failid.append(df['visitid'][j])
                elif cutoff == '2+':
                    if geturineprotine(df, j) in ['2+','3+','4+']:
                        num+=1
                        visitid.append(df['visitid'][j])
                    else:
                        fail+=1
                        failid.append(df['visitid'][j])
            elif geturineprotine(df, j) in non:
                no+=1
            else: 
                cannot+=1
                unpred.append(df['visitid'][j])
                
        if num >= time:
            result = True
            comment = 'urine protein >= '+cutoff+' for '+str(num)+' times at visitid '+', '.join(visitid)
        else: 
            result = False
            if num > 0:
                comment = 'urine protein >= '+cutoff+' for only '+str(num)+' time at visitid '+', '.join(visitid)
                if cannot > 0: comment = comment+' and are not from test strip at visitid '+', '.join(unpred)
                if fail > 0: comment = comment+' and < '+cutoff+' at visitid '+', '.join(failid)
            elif no == len(histindex):
                comment = 'No urine protein lab results recorded'
            elif cannot == len(histindex):
                comment = 'All urine protein lab results are not from test strip at visitid '+', '.join(unpred)
            elif fail == len(histindex):
                comment = 'All urine protein lab results are < '+cutoff+' at visitid '+', '.join(failid)
            elif fail > 0 and cannot == 0:
                comment = 'Urine protein lab results are < '+cutoff+' at visitid '+', '.join(failid)
            elif cannot > 0 and fail == 0:
                comment = 'Urine protein lab results are not from test strip at visitid '+', '.join(unpred)
            else:
                comment = 'Urine protein lab results are < '+cutoff+' at visitid '+', '.join(failid)+' and are not from test strip at visitid '+', '.join(unpred)
        return [result, comment]
    
    # urine wbc
    notinfect=['notfound','ไม่มี','0-1','1-2','2-3','3-5']
    infect=['5-10','10-20','20-30','30-50','50-100','>100','100-200','numerous','over100']
    
    def geturinewbc(df, index):
        if type(df['lab'][index]) == list and len(df['lab'][index]) > 0:
            for dic in df['lab'][index]:
                ln = dic['lab_name'].lower()
                rn = dic['result_name'].lower()
                if 'urine' in ln and 'wbc' in rn:
                    return dic['result'].lower().replace(' ','').replace('  ','').replace('/hpf','').replace('cell','')
                elif df['lab'][index][-1] == dic:
                    return 'none'
        else:
            return 'none'
    
    # urine microalbumin
    def getmicroalbumin(df, index):
        
        def extractnum(string):
            import re
            return re.sub("[^0-9\.<>]", "", string)
        def removeparen(string):
            if ('(' or ')') in string:
                string = re.sub('\(.*\)', "", string)
            if ('[' or ']') in string:
                string = re.sub('\[.*\]', "", string)
            return string
        def removesign(string):
            result = string
            if result != '':
                if '<' in result:
                    result = float(string.replace('<',''))-1
                if '>' in result:
                    result = float(result.replace('>',''))+1
                result = float(result)
                return result
            else:
                return 'none'
        if type(df['lab'][index]) == list and len(df['lab'][index]) > 0:
            for dic in df['lab'][index]:
                ln = dic['lab_name'].lower()
                rn = dic['result_name'].lower()
                if 'microalbumin' in ln and ('microalbumin' in rn or 'mau' in rn):
                    return removesign(extractnum(removeparen(dic['result'])))
                elif df['lab'][index][-1] == dic:
                    return 'none'
        else:
            return 'none'
    
    def urinemicroalbumin_num(df, index, cutoff, time):      
        yes = []
        fail = []
        no = []
        for j in histindex:
            if getmicroalbumin(df, j) not in non:
                if getmicroalbumin(df, j) >= cutoff:
                    yes.append(df['visitid'][j])
                else: fail.append(df['visitid'][j])
            else: no.append(df['visitid'][j])
        if len(yes) >= time:
            result = True
            comment = 'Urine MAU >= '+str(cutoff)+' for '+str(len(yes))+' times at visitid '+', '.join(yes)
        else:
            result = False
            if len(yes) > 0:
                comment = 'Urine MAU >= '+str(cutoff)+' for only '+str(len(yes))+' times at visitid '+', '.join(yes)
                if len(fail) > 0:
                    comment = comment+' and are < '+str(cutoff)+' for '+str(len(fail))+' times at visitid '+', '.join(fail)
            elif len(no) == len(histindex):
                comment = 'No Urine MAU lab results recorded'
            else: comment = 'Urine MAU < '+str(cutoff)+' for '+str(len(fail))+' times at visitid '+', '.join(fail)
        return [result, comment]
        
    # urine creatinine
    def geturinecreatinine(df, index):
        def extractnum(string):
            import re
            return re.sub("[^0-9\.<>]", "", string)
        def removeparen(string):
            if ('(' or ')') in string:
                string = re.sub('\(.*\)', "", string)
            if ('[' or ']') in string:
                string = re.sub('\[.*\]', "", string)
            return string
        def removesign(string):
            result = string
            if result != '':
                if '<' in result:
                    result = float(string.replace('<',''))-1
                if '>' in result:
                    result = float(string.replace('>',''))+1
                result = float(result)
                return result
            else:
                return 'none'
        if type(df['lab'][index]) == list and len(df['lab'][index]) > 0:
            for dic in df['lab'][index]:
                ln = dic['lab_name'].lower()
                rn = dic['result_name'].lower()
                if ('urine' in ln or 'microalbumin' in ln) and 'creatinine' in rn:
                    return removesign(extractnum(removeparen(dic['result'])))
                elif df['lab'][index][-1] == dic:
                    return 'none'
        else:
            return 'none'
        
    def mau_per_crtn_num(df, index, cutoff, time):
        yes = []
        fail = []
        mauonly = []
        crtnonly = []
        for j in histindex:
            if getmicroalbumin(df, j) not in non and geturinecreatinine(df, j) in non:
                mauonly.append(df['visitid'][j])
            if geturinecreatinine(df, j)not in non and getmicroalbumin(df, j) in non:
                crtnonly.append(df['visitid'][j])
            if getmicroalbumin(df, j) not in non and geturinecreatinine(df, j) not in non:
                if getmicroalbumin(df, j)/geturinecreatinine(df, j) >= cutoff:
                    yes.append(df['visitid'][j])
                else: fail.append(df['visitid'][j])                    
        if len(yes) >= time:
            result = True
            comment = 'Ratio >= '+str(cutoff)+' for '+str(len(yes))+' times at visitid '+', '.join(yes)
        else:
            result = False
            if len(yes) > 0:
                comment = 'Ratio >= '+str(cutoff)+' for only '+str(len(yes))+' times at visitid '+', '.join(yes)
                if len(fail) > 0:
                    comment = comment+' and are < '+str(cutoff)+' for '+str(len(fail))+' times at visitid '+', '.join(fail)
                if len(mauonly) > 0:
                    comment = comment+' and there is only urine microalbumin at visitid '+', '.join(mauonly)
                if len(crtnonly) > 0:
                    comment = comment+' and there is only urine creatinine at visitid '+', '.join(crtnonly)
            elif len(fail) > 0:
                comment = 'Ratio < '+str(cutoff)+' for '+str(len(fail))+' times at visitid '+', '.join(fail)
                if len(mauonly) > 0:
                    comment = comment+' and there is only urine microalbumin at visitid '+', '.join(mauonly)
                if len(crtnonly) > 0:
                    comment = comment+' and there is only urine creatinine at visitid '+', '.join(crtnonly)
            elif len(mauonly) > 0:
                comment = 'There is only urine microalbumin at visitid '+', '.join(mauonly)
                if len(crtnonly) > 0:
                    comment = comment+' and only urine creatinine at visitid '+', '.join(crtnonly)
            elif len(crtnonly) > 0:
                comment = 'There is only urine creatinine at visitid '+', '.join(crtnonly)
            else: comment = 'No urine microalbumin and urine creatinine lab results recorded'
        return [result, comment]  

    # 3. n083 test--------------------------------------------------------------------------------#
    
    def n083(df, index):
        i = 0
        for column in df.columns:
            if type(df[column][index]) == str:
                if "N083" in df[column][index].split(';') or "N08.3" in df[column][index].split(';'):
                    return [True, 'in '+str(column).replace('_',' ')]
                elif i == len(df.columns)-3:
                    return [False]
                else:
                    i+=1
    
    #---------------------------------------------------------------------------------------------#
    
    path = {}
    
    #path1: มีการบันทึก(ประวัติเดิม)ว่าเป็น DM with renal complication และมีการบันทึกใหม่ว่าเป็นและ Urine protein>=1+ อย่างน้อย1ครั้ง
    
    #Defined key variables
    path1 = 'Option 1'
    history_renal = 'DM with Renal history recorded'
    urine_protein_1 = 'Urine Protein >= 1+ for >= 1 times'
    urine_protein_2 = 'Urine Protein >= 2+ for >= 2 times'
    urine_no_infect = 'No Urinary Tract Infection history'
    n08_3 = 'N083 Code'
    
    path[path1]={}
    
    # มีการบันทึก(ประวัติเดิม)ว่าเป็น DM with renal complication
    path[path1][history_renal] = histrenal(df, index)
        
    # Urine protein >= 1+ อย่างน้อย 1 ครั้ง
    path[path1][urine_protein_1] = urineprotein_num(df, index, '1+', 1)
        
    # ไม่มีการติดเชื้อ (urine WBC < 5-10)
    clean = []
    no_wbc = 0
    for j in histindex:
        if geturinewbc(df, j) in infect or ('100' or '200') in  geturinewbc(df, j):
            path[path1][urine_no_infect] = [False,'WBC > 10 at visitid '+str(df['visitid'][j])]
            break
        elif geturinewbc(df, j) in notinfect:
            clean.append(df['visitid'][j])
        else: no_wbc+=1
    if no_wbc == len(histindex):
        path[path1][urine_no_infect] = [True, 'No urine WBC lab results recorded']
    elif no_wbc+len(clean) == len(histindex):
        path[path1][urine_no_infect] = [True, 'All urine WBC recorded are < 5 at visitid '+', '.join(clean)]
        
    # กรอก N083
    path[path1][n08_3] = n083(df, index)
    
    #---------------------------------------------------------------------------------------------#
    if len(set([x[0] for x in path[path1].values()])) == 1 and True in set([x[0] for x in path[path1].values()]):
        return {'result': 'Pass', 'detail': path[path1]}
    else:
    
    # path2: Urine protein>=2+ อย่างน้อย 2 ครั้ง และต้องไม่มีการติดเชื้อ (urine WBC < 5-10)
        
        #Defined variables
        path2 = 'Option 2'

        path[path2]={}

        # Urine protein 2+ อย่างน้อย 2 ครั้ง
        path[path2][urine_protein_2] = urineprotein_num(df, index, '2+', 2)

        # ไม่มีการติดเชื้อ (urine WBC < 5-10)
        path[path2][urine_no_infect] = path[path1][urine_no_infect]
        
        # กรอก N083
        path[path2][n08_3] = path[path1][n08_3]
        
    #---------------------------------------------------------------------------------------------#
        
        if len(set([x[0] for x in path[path2].values()])) == 1 and True in set([x[0] for x in path[path2].values()]):
            return {'result': 'Pass', 'detail': path[path2]}
        else:
        
    # path3: Urine microalbumin +ve (>=30) 2 ครั้ง และต้องไม่มีการติดเชื้อ (urine WBC < 5-10)
            path3 = 'Option 3'
            urine_microalbumin_2 = 'Urine Microalbumin >= 30 for >= 2 times'

            path[path3]={}

            # Urine microalbumin +ve (>30) 2 ครั้ง
            path[path3][urine_microalbumin_2] = urinemicroalbumin_num(df, index, 30, 2)

            # ไม่มีการติดเชื้อ (urine WBC < 5-10)
            path[path3][urine_no_infect] = path[path1][urine_no_infect]
            
            # กรอก N083
            path[path3][n08_3] = path[path1][n08_3]
    
    #---------------------------------------------------------------------------------------------#
    
            if len(set([x[0] for x in path[path3].values()])) == 1 and True in set([x[0] for x in path[path3].values()]):
                return {'result': 'Pass', 'detail': path[path3]}
            else:
    
    #path4: Urine microalbumin/creatinine ratio > 30mg/g 2 ครั้ง และต้องไม่มีการติดเชื้อ (urine WBC < 5-10)
                path4 = 'Option 4'
                microalbumin_creatinine_2 = 'Urine Microalbumin/Creatinine Ratio >= 30 for >= 2 times'

                path[path4]={}

                # Urine microalbumin/creatinine ratio > 30mg/g อย่างน้อย 2 ครั้ง
                path[path4][microalbumin_creatinine_2] = mau_per_crtn_num(df, index, 30, 2)

                # ไม่มีการติดเชื้อ (urine WBC < 5-10)
                path[path4][urine_no_infect] = path[path1][urine_no_infect]
                
                # กรอก N083
                path[path4][n08_3] = path[path1][n08_3]
    
    #---------------------------------------------------------------------------------------------#
                
                if len(set([x[0] for x in path[path4].values()])) == 1 and True in set([x[0] for x in path[path4].values()]):
                    return {'result': 'Pass', 'detail': path[path4]}
                else:
                    return {'result': 'Fail', 'detail': path}